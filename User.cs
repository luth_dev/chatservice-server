﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;

namespace ChatService
{
    // The User class contains data such as username, id, permissions etc. that's stored in a DB
    class User
    {
        public User(int id, string username)
        {
            Username = username;
            Id = id;
        }

        int Id;
        string Username;
        //private List<Permission> Permissions; extend this in the future with Permission scopes (group permission, global permission etc.)
        //private string SessionToken;
    }

    // The Session class contains data about the connection between the user and the server
    class Session
    {
        Session(Socket socket)
        {
            _socket = socket;
        }

        //public bool Alive { get;  private set; } Implement Alive member after networking and threading is done because I can't figure out how to implement it
        private Socket _socket;
        private User _user;
    }

    /* Planned features: Roles, Permissions, SessionToken used to reconnect
    enum Permission
    {
        PERM_ADMIN,
        PERM_BAN_USER,
        PERM_MUTE_USER,
        PERM_DELETE_MESSAGE,
        PERM_EDIT_MESSAGE,
        PERM_SEND_MESSAGE
    }

    struct SessionToken
    {
        Guid Token;
        DateTime CreationTime;
        DateTime ExpirationTime;
    }

    
    enum Roles
    {
        ROLE_ADMIN,
        ROLE_DEVELOPER,
        ROLE_MODERATOR,
        ROLE_SOMETHING
    }
     */
}
