﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Security;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace ChatService
{
    public class Server
    {
        public static ManualResetEvent ev = new ManualResetEvent(false);
        private X509Certificate serverCertificate = null;
        private TcpListener _listener;

        public Server(IPAddress ip, int port)
        {
            //LoadCertificateFromFile(certPath);
            
            _listener = new TcpListener(ip, port);
            Log.Info("Server started on IP {0} port {1}", ip, port);
            Initiate();
        }

        private void Initiate()
        {
            _listener.Start();
            while (true)
            {
                ev.Reset();
                _listener.BeginAcceptTcpClient(new AsyncCallback(AcceptCallback), _listener);
                ev.WaitOne();
            }
        }
        List<Socket> ssls = new List<Socket>();
        public void AcceptCallback(IAsyncResult ar)
        {
            TcpClient handler = _listener.AcceptTcpClient();
            Log.Info("Added socket from IP {0}", handler.Client.RemoteEndPoint);
            ssls.Add(handler.Client);
            Log.Info("Total number of sockets: {0}", ssls.Count);
            ev.Set();
        }

        private void LoadCertificateFromFile(string cert)
        {
            serverCertificate = X509Certificate.CreateFromCertFile(cert);

        }
    }

    public class ConnectionManager
    {
        List<Session> SessionList;

    }
}

