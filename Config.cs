﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace ChatService
{
    static class Config
    {
        // Set default values here using an initializer list 
        static private Dictionary<string, dynamic> _configStore = new Dictionary<string, dynamic>
        {
            { "hostname", "127.0.0.1" },                            // Ip address that the server will bind to, use 0.0.0.0 to bind to all interfaces
            { "port", 5147},                                        // Port that the server will bind to
            { "secure_mode", false },                               // Use SSL, NOT YET IMPLEMENTED
            { "motd", "Welcome to a ChatService powered server!" }, // Message shown to the clients after logging in
            { "allow_anonymous", false }                            // Allow users to login as anonymous and give them a GUID 
        };

        // Generic function to get value of a specified config option; returns an empty string 
        static public bool GetOption<T>(string optionName, out T option)
        {
            option = default(T);
            if (!_configStore.ContainsKey(optionName))
            {
                Log.Error("Config option '{0}' doesn't exist!", optionName);
                return false;
            }
            if (_configStore[optionName].GetType() != typeof(T))
            {
                Log.Error("Invalid GetOption type of {0}, expected {1}", typeof(T), _configStore[optionName].GetType());
                return false;
            }
            option = (T)_configStore[optionName];
            return true;
        }

        // Enumerate current options in a fancy way
        static public void EnumOptions()
        {
            Console.WriteLine("Current configuration: ");
            foreach (KeyValuePair<string, dynamic> kvp in _configStore)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(kvp.Key);
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(" {0}\n", kvp.Value);
            }
            // Set the foreground color back to default
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        static public void LoadFromFile(string path)
        {
            // Check if config exists and if it doesn't just return and use the default values
            if (!File.Exists(path))
            {
                Log.Warning("Config file '{0}' doesn't exist! Using default config values.", path);
                return;
            }
            // Use a stream reader to store a line in a temp string 'line' and then pass it over to ParseLine method
            // Also pass the file number (stored in cnt) in order to allow ParseLine to output the line number in the log if something goes wrong
            using (StreamReader file = new StreamReader(path))
            {
                string line;
                int cnt = 1;
                while ((line = file.ReadLine()) != null)
                {
                    ParseLine(line, cnt);
                    ++cnt;
                }
            }
        }

        // Parse a line, check if syntax is correct and load the given config value
        // TODO: The code itself feels a bit messy though so look to improve it in the future
        // Implement a mechanism that allows the addition of a validation rule for each config key-value pair (RegEx maybe?)
        static private void ParseLine(string line, int num)
        {
            if (line.Length == 0)
            {
                Log.Error("Found empty line at {0}", num);
                return;
            }
            // If the line begins with a # then it will count as a comment and it will be ignored 
            if (line[0] == '#')
                return;
            // Split the line at the given '=' and then count the substrings, we expect only 2 of them, the key and the value
            string[] substr = line.Split('=');
            if (substr.Length != 2)
            {
                Log.Error("Invalid syntax at line {0}. Syntax is Key=Value.", num);
                return;
            }
            // Check if the given key exists in the configStore dictionary, and if it doesn't log the error and return
            if (!_configStore.ContainsKey(substr[0]))
            {
                Log.Error("Invalid Key {0} at line {1}", substr[0], num);
                return;
            }
            dynamic tempValue;
            // Attempt to convert the value substr[1] to the type of the default value with the key substr[0] and log any exceptions
            try
            {
                tempValue = Convert.ChangeType(substr[1], _configStore[substr[0]].GetType());
            }
            catch (Exception e)
            {
                Log.Error("Invalid value {0} for key {1}. Exception: {2} ", substr[1], substr[0], e.Message);
                return;
            }
            // And finally if everything goes well assign the converted value to the value with the given key
            _configStore[substr[0]] = tempValue;
            Log.Debug("Loaded '{0}' as '{1}'", tempValue, substr[0]);
        }
    }
}