﻿using System;

namespace ChatService
{
    public enum LogType
    {
        LOGTYPE_ERROR,
        LOGTYPE_WARNING,
        LOGTYPE_INFO,
        LOGTYPE_DEBUG,
        LOGTYPE_ALL
    }

    // ToDo: Check if Console.ForegroundColor works on Linux and add some fancy colros if it does
    static public class Log
    {
        // Helper log builder function
        static private void LogBuilder(LogType type, string message, params object[] values)
        {
            string logType = "";
            string formatMessage;
            switch (type)
            {
                case LogType.LOGTYPE_ERROR:
                    logType = "ERROR";
                    break;
                case LogType.LOGTYPE_WARNING:
                    logType = "WARNING";
                    break;
                case LogType.LOGTYPE_INFO:
                    logType = "INFO";
                    break;
                case LogType.LOGTYPE_DEBUG:
                    logType = "DEBUG";
                    break;
                default:
                    logType = "UNKNOWN";
                    break;
            }
            try
            {
                formatMessage = new string(String.Format(message, values));
            }
            catch
            {
                formatMessage = "Wrong string format!";
            }
            string finalMessage = String.Format("[{0}][{1}]: {2}", DateTime.Now.ToString("dd-MM-yyyy H:mm:ss"), logType, formatMessage);
            LogConsoleOut(finalMessage);            
        }

        // All of these functions are the front end of the Log class; they call the LogBuilder function
        static public void Error(string message, params object[] values)
        {
            LogBuilder(LogType.LOGTYPE_ERROR, message, values);
        }

        static public void Warning(string message, params object[] values)
        {
            LogBuilder(LogType.LOGTYPE_WARNING, message, values);
        }

        static public void Info(string message, params object[] values)
        {
            LogBuilder(LogType.LOGTYPE_INFO, message, values);
        }

        static public void Debug(string message, params object[] values)
        {
            LogBuilder(LogType.LOGTYPE_DEBUG, message, values);
        }

        static private void LogConsoleOut(string message)
        {
            Console.WriteLine(message);
        }
    }
}


