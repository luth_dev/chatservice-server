﻿using System;
using System.Net;

namespace ChatService
{

    class Program
    {
        static void Main(string[] args)
        {
            Log.Info("Starting program!");
            // Look for a '-c' argument in args and check if the next arg is a path, else ignore; hackfix, requires proper parsing of arguments
            for (int i = 0; i < args.Length - 1; ++i)
                if (args[i] == "-c")
                {
                    // If arg -c exists, try to use next arg as a path and break the loop
                    Config.LoadFromFile(args[i + 1]);
                    break;
                }
            Config.EnumOptions();
            string ipAddress;
            int port = 0;
            if (!Config.GetOption<string>("hostname", out ipAddress) || !Config.GetOption<int>("port", out port))
            {
                Log.Error("Error while getting config value!");
                Console.ReadKey();
                Environment.Exit(1);
            }
            Server srv = new Server(IPAddress.Parse(ipAddress), port);
            Console.ReadKey();
        }
    }
}
